﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_RieraMarc
{
    class MyVector
    {
        public int[] inici { get; set; }
        public int[] final { get; set; }


        public MyVector(int[] iniciRandom, int[] finalRandom)
        {
            inici = iniciRandom;
            final = finalRandom;


        }


        public double Distancia()
        {
            return Math.Sqrt(Math.Pow(inici[0] - inici[1], 2) + Math.Pow(final[0] - final[1], 2));
        }

        public void Direccio(MyVector[] array)
        {

        }


        public override string ToString()
        {
            return "Punt inicial: {" + inici[0] + ", " + inici[1] + "}, Punt final: {" + final[0] + ", " + final[1] + "}";
        }


    }
}
