﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_RieraMarc
{
    class VectorGame
    {
        public MyVector[] random(int num)
        {

            MyVector[] randomVectors = new MyVector[num];
            for (int x = 0; x < randomVectors.Length; x++)
            {
                randomVectors[x] = new MyVector(randomVector(-50, 50), randomVector(-50, 50));
            }
            Console.WriteLine("Creat array de vectors random");
            return randomVectors;
        }
        private int[] randomVector(int minim, int maxim)
        {
            Random rnd = new Random();

            return new int[] { rnd.Next(minim, maxim), rnd.Next(minim, maxim) };
        }

        public MyVector[] SortVectors(MyVector[] array)
        {
            MyVector[] cambiVec;
            cambiVec = array;
            int x = 0;

            foreach (MyVector vector1 in array)
            {
                double distancia1 = array[x].Distancia();
                for (int y = x; y < array.Length; y++)
                {
                    double distancia2 = array[y].Distancia();
                    if (distancia1 < distancia2)
                    {
                        cambiVec[0] = array[x];
                        array[x] = array[y];
                        array[y] = cambiVec[0];
                        distancia1 = distancia2;

                    }

                }
                x = x + 1;
            }

            return array;
        }



        public void showArray(MyVector[] arrayVectors)
        {
            int x = 0;
            Console.WriteLine("Vectors:");
            foreach (MyVector vector in arrayVectors)
            {

                Console.WriteLine("Vector: {0}  Coordenades:\t{1}    Distancia: \t{2}", x, vector.ToString(), vector.Distancia());
                x++;
            }
        }


    }
}
